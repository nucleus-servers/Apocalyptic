/*
 * Copyright (C) 2015 Kaisar Arkhan
 * 
 * This file is part of Apocalyptic.
 * 
 * Apocalyptic is free software: you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * Apocalyptic is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Apocalyptic. If not,
 * see <http://www.gnu.org/licenses/>.
 */

package net.cyberninjapiggy.apocalyptic.api;

import net.cyberninjapiggy.apocalyptic.api.enums.RadiationChangeCause;
import net.cyberninjapiggy.apocalyptic.misc.RadiationManager;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.UUID;

/**
 * ApocalypticAPI
 * <p>
 * This class is made for external / third-party plugins to interact
 * with this plugin.
 *
 * @author Kaisar Arkhan <yuki@airmail.cc>
 */
public class ApocalypticAPI {
    private static ApocalypticAPI instance;
    private RadiationManager radiationManager;

    public ApocalypticAPI(RadiationManager radiationManager) {
        this.radiationManager = radiationManager;

        instance = this;
    }

    public static ApocalypticAPI getInstance() {
        return instance;
    }

    /**
     * Adds an amount of radiation to the player
     * <p>
     * You can use negative numbers to subtract.
     *
     * @param p         The Player
     * @param radiation Amount of Radiation you want to add.
     */
    public void addRadiation(Player p, double radiation) {
        radiationManager.addPlayerRadiation(p, radiation, RadiationChangeCause.API);
    }

    /**
     * Adds an amount of radiation to the player
     * <p>
     * You can use negative numbers to subtract.
     *
     * @param id        The Player's UUID
     * @param radiation Amount of Radiation you want to add.
     */
    public void addRadiation(UUID id, double radiation) {
        addRadiation(Bukkit.getPlayer(id), radiation);
    }

    /**
     * Sets an amount of radiation to the Player
     *
     * @param p         The Player
     * @param radiation Amount of Radiation you want to set
     */
    public void setRadiation(Player p, double radiation) {
        radiationManager.setPlayerRadiation(p, radiation, RadiationChangeCause.API);
    }

    /**
     * Sets an amount of radiation to the Player
     *
     * @param id        The Player's UUID
     * @param radiation Amount of Radiation you want to set
     */
    public void setRadiation(UUID id, double radiation) {
        setRadiation(Bukkit.getPlayer(id), radiation);
    }

    /**
     * Get The Player's Radiation Level
     *
     * @param p The Player
     * @return The Player's Radiation Level
     */
    public double getPlayerRadiation(Player p) {
        return radiationManager.getPlayerRadiation(p);
    }

    /**
     * Get The Player's Radiation Level
     *
     * @param id The Player's UUID
     * @return The Player's Radiation Level
     */
    public double getPlayerRadiation(UUID id) {
        return getPlayerRadiation(Bukkit.getPlayer(id));
    }
}
