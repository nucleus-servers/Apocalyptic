package net.cyberninjapiggy.apocalyptic.api.enums;

public enum RadiationChangeCause {
    NATURAL, EAT, WASHED, DIED, API, COMMAND
}
