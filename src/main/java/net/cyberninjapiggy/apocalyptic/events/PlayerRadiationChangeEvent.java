package net.cyberninjapiggy.apocalyptic.events;

import net.cyberninjapiggy.apocalyptic.enums.RadiationChangeCause;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

@Deprecated
public class PlayerRadiationChangeEvent extends PlayerEvent implements Cancellable {

    private static final HandlerList handlers = new HandlerList();

    private double radiationNew;
    private double radiationOld;

    private boolean cancelled;

    private RadiationChangeCause cause;

    public PlayerRadiationChangeEvent(Player who, double radiationNew, double radiationOld,
                                      RadiationChangeCause cause) {
        super(who);

        this.radiationNew = radiationNew;
        this.radiationOld = radiationOld;

        this.cause = cause;
    }

    public static HandlerList getHandlersList() {
        return handlers;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean cancel) {
        this.cancelled = cancel;
    }

    public double getNewRadiation() {
        return radiationNew;
    }

    public double getOldRadiation() {
        return radiationOld;
    }

    public void setRadiation(double radiation) {
        this.radiationNew = radiation;
    }

    public RadiationChangeCause getCause() {
        return cause;
    }

}
