/*
 * Copyright (C) 2015 Kaisar Arkhan
 * Copyright (C) 2014 Nick Schatz
 * 
 * This file is part of Apocalyptic.
 * 
 * Apocalyptic is free software: you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * Apocalyptic is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Apocalyptic. If not,
 * see <http://www.gnu.org/licenses/>.
 */

package net.cyberninjapiggy.apocalyptic.listeners;

import net.cyberninjapiggy.apocalyptic.Apocalyptic;
import net.minecraft.server.v1_8_R3.AttributeInstance;
import net.minecraft.server.v1_8_R3.AttributeModifier;
import net.minecraft.server.v1_8_R3.EntityInsentient;
import net.minecraft.server.v1_8_R3.GenericAttributes;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftEntity;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.inventory.ItemStack;

import java.util.UUID;

public class MonsterSpawn implements Listener {
    private final Apocalyptic plugin;
    private final UUID zombieSpeedUUID = UUID.fromString("fb972eb0-b792-4ec3-b255-5740974f6eed");

    public static int HORDE_SPREAD_MIN = 4;
    public static int HORDE_SPREAD_MAX = 10;
    public static int HORDESIZE_SPREADGROUPING_MIN = 1;
    public static int HORDESIZE_SPREADGROUPING_MAX = 5;

    public MonsterSpawn(Apocalyptic plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onEntitySpawn(CreatureSpawnEvent event) {
        LivingEntity entity = event.getEntity();

        if (entity.getType() == EntityType.ZOMBIE
                && plugin.worldEnabledZombie(entity.getLocation().getWorld().getName())) {
            EntityInsentient nmsEntity = (EntityInsentient) ((CraftEntity) entity).getHandle();

            AttributeInstance attributes =
                    nmsEntity.getAttributeInstance(GenericAttributes.MOVEMENT_SPEED);

            AttributeModifier modifier =
                    new AttributeModifier(zombieSpeedUUID,
                            "Apocalyptic movement speed modifier", plugin.getConfig()
                            .getWorld(event.getEntity().getWorld()).getDouble("mobs.zombies.speedMultiplier"),
                            1);

            attributes.b(modifier);
            attributes.a(modifier);
        }
    }

    @EventHandler
    public void onMonsterSpawn(CreatureSpawnEvent e) {
        if (e.getEntityType() == EntityType.ZOMBIE
                && plugin.worldEnabledZombie(e.getLocation().getWorld().getName())) {

            int zombieCount = e.getEntity().getWorld().getEntitiesByClass(Zombie.class).size();
            // Check if the limit of spawning zombies has reached
            if (e.getEntity().getWorld().getEntitiesByClass(Zombie.class).size() >= plugin.getConfig()
                    .getWorld(e.getLocation().getWorld()).getInt("mobs.zombies.spawnLimit")) {
                e.setCancelled(true);
                return;
            }

            // Get the location of the zombie
            Location l = e.getLocation();

            // 1/300 chance of spawning mutant zombie
            if (plugin.getRandom().nextInt(300) == 0 && plugin.getConfig()
                    .getWorld(e.getLocation().getWorld()).getBoolean("mobs.mutants.zombie")) {
                e.setCancelled(true);
                l.getWorld().spawnEntity(l, EntityType.GIANT);
                return;
            }

            // Set the health and the max health of the zombie
            e.getEntity().setMaxHealth(
                    plugin.getConfig().getWorld(e.getEntity().getWorld()).getDouble("mobs.zombies.health"));
            e.getEntity().setHealth(
                    plugin.getConfig().getWorld(e.getEntity().getWorld()).getDouble("mobs.zombies.health"));

            // Check if the zombie is NOT being spawned manually/from a spawner
            if (e.getSpawnReason() != SpawnReason.CUSTOM && e.getSpawnReason() != SpawnReason.SPAWNER) {
                int minHordeSize = plugin.getConfig().getWorld(e.getEntity().getWorld()).getInt("mobs.zombies.hordeSize.min");
                int maxHordeSize = plugin.getConfig().getWorld(e.getEntity().getWorld()).getInt("mobs.zombies.hordeSize.max");

                int hordeSize = plugin.getRandom().nextInt(maxHordeSize - minHordeSize) + minHordeSize;

                hordeSize = (int) Math.ceil(Math.abs(hordeSize - (2 + Math.cos(zombieCount / 125))));

                long time = e.getEntity().getWorld().getTime();

                // NIGHTTIME 23:00~ or 24:00~05:00, or 23:00~05:00
                if (time >= 17000 || (time >= 18000 && time <= 23000)) {
                    hordeSize += hordeSize * 2;
                }

                Player nearPlayer = null;
                for (Entity entity : e.getEntity().getWorld().getNearbyEntities(l, 100, 100, 100)) {
                    if (entity instanceof Player) {
                        nearPlayer = (Player) entity;
                        break;
                    }
                }

                if (nearPlayer == null) {
                    spawnZombie(l, hordeSize);
                    return; // Spawn the modified zombie
                } else {
                    int offsetX = plugin.getRandom().nextInt(48) + 16;
                    int offsetZ = plugin.getRandom().nextInt(48) + 16;

                    Location farLocation = adjustSafeLocation(nearPlayer.getLocation(), offsetX, 0, offsetZ);

                    if (nearPlayer.getLocation().distance(farLocation) > 32) {
                        spawnZombie(farLocation, hordeSize);
                    } else {
                        //Cannot spawn zombie
                    }
                }
            }

        }

        if (e.getEntityType() == EntityType.CREEPER) {
            if (plugin.getConfig()
                    .getBoolean("worlds." + e.getLocation().getWorld().getName() + ".mobs.mutants.creeper")) {
                if (plugin.getRandom().nextInt(100) == 0) {
                    ((Creeper) e.getEntity()).setPowered(true);
                    return;
                }
            }
        }

        if (e.getEntityType() == EntityType.SKELETON) {
            if (plugin.getConfig().getBoolean(
                    "worlds." + e.getLocation().getWorld().getName() + ".mobs.mutants.skeleton")) {
                if (plugin.getRandom().nextInt(100) == 0) {
                    ((Skeleton) e.getEntity()).setSkeletonType(Skeleton.SkeletonType.WITHER);
                    e.getEntity().getEquipment().setItemInHand(new ItemStack(Material.IRON_SWORD, 0));
                }
            }
        }
    }

    private void spawnZombie(Location farLocation, int hordeSize) {
        int spreadGroup = HORDESIZE_SPREADGROUPING_MIN;
        if (hordeSize / HORDESIZE_SPREADGROUPING_MAX > 0) { //making sure that the zombies can spread-group as best as they can
            spreadGroup = plugin.getRandom().nextInt(HORDESIZE_SPREADGROUPING_MAX - HORDESIZE_SPREADGROUPING_MIN) + HORDESIZE_SPREADGROUPING_MIN;
        }

        int zombiesPerGroup = hordeSize / spreadGroup;

        for (int group = 1; group <= spreadGroup; group++) {
            for (int hordeAmount = 1; hordeAmount <= zombiesPerGroup; hordeAmount++) {
                Location spreadLocation = adjustSafeLocation(farLocation, getRandomHordeSpread(), 0, getRandomHordeSpread());

                Zombie zombie = (Zombie) spreadLocation.getWorld().spawnEntity(farLocation, EntityType.ZOMBIE);
                EntityEquipment equipment = zombie.getEquipment();

                if (equipment.getHelmet() != null && !zombie.isBaby() && !plugin.getConfig()
                        .getWorld(zombie.getWorld()).getBoolean("mobs.zombies.burnInDaylight")) {
                    ItemStack head = new ItemStack(Material.SKULL_ITEM, 1, (byte) 2);
                    equipment.setHelmet(head);
                    equipment.setHelmetDropChance(0f);
                }
            }
        }
    }

    private Location adjustSafeLocation(Location loc, int x, int y, int z) {
        Location safeLocation = loc.add(x, y, z);
        safeLocation.setY(safeLocation.getWorld().getHighestBlockYAt(safeLocation));

        return safeLocation;
    }

    private int getRandomHordeSpread() {
        return plugin.getRandom().nextInt(HORDE_SPREAD_MAX - HORDE_SPREAD_MIN) + HORDE_SPREAD_MIN;
    }
}
