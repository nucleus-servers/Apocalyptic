# Apocalyptic [![Build Status](https://drone.io/github.com/captainfroster/Apocalyptic/status.png)](https://drone.io/github.com/captainfroster/Apocalyptic/latest)

Apocalyptic is a Bukkit Plugin that adds Nuclear / Zombie Post-Apocalypse elements to your server.

## Downloads / Releases
* You can download a Stable Build / Production-Ready Build from [here](https://github.com/captainfroster/Apocalyptic/releases)
* You can download a Bleeding-Edge Development Build of this Project from [here](https://drone.io/github.com/captainfroster/Apocalyptic/files) ( __WARNING:__ May Break your Server. __NEVER__ Use a Development Build for a Production Server ( a Server that have a high volume of players ) )

## Installation

Download Apocalyptic.jar and add to your /plugins/ folder.
If you want world generation, add this snippet to the bottom of bukkit.yml.
```
worlds:
  <world>:
    generator: Apocalyptic
```
Where `<world>` is the name of the world you want to generate.

If you use Multiverse, run this command to make a new Apocalyptic world.

`/mv create <world> normal -g Apocalyptic`

Again, where `<world>` is the name of the world you want to generate.

Run the server. This will generate some of the world and create config files.

Done! Have fun!

## Developer API
We have a Developer API that you can use for your plugins to interact with Apocalyptic wihout breaking it. [Read this](https://github.com/captainfroster/Apocalyptic/wiki/Developer-API)

## To-do List
- [x] Make it Compatible with 1.8
- [X] Use PreparedStatements instead of Plain Query in Database Updates
- [X] Add support for External Database ( MySQL )
- [X] Clean the Code ( again )
- [X] Better Zombie spawn picking
- [X] Fix Zombie Spawn checker or Fix Zombie spawn picking?
- [ ] Natural spawning for sugarcane and cactus

## For Your Information
>I won't create a BukkitDev / Curse's Minecraft Bukkit Plugin Page, because I don't have the time to update. If I do, The Page will most likely abandoned or lack of updates.

>This is a forked version of [epicfacecreeper's Apocalyptic](https://github.com/epicfacecreeper/Apocalyptic). I created this fork because the original project looks like It's not being maintained ( Last Commit Aug 11, 2014 ), because My Server is using this plugin I decided to modify this plugin to be 1.8 Compatible.

## Bugs? Issues? Feature Requests?
If you want to report a Bug, Issues, or a Feature , Please use GitHub's Issue Management System.
